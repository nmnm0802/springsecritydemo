package com.example.demo.Security;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.example.demo.Repository.UserRepository;
import com.example.demo.entity.User;

@Service
public class LoginUserDetailsService implements UserDetailsService {


    @Autowired
    UserRepository userRepository;
    @Autowired
    private HttpServletRequest request;

    @Override
    public UserDetails loadUserByUsername(String name) throws UsernameNotFoundException {
        //入力された名前をキーにemployeeテーブルのレコードを1件取得
        User user = userRepository.findUser(name);

        //該当レコードが取得できなかった場合はエラーにする
        if  (user  ==  null )   {
            throw new AuthenticationCredentialsNotFoundException("アカウントまたはパスワードが誤っています");
        }
        String role = "ROLE_ADMIN";

        return new LoginUserDetails(user, role);
    }
}